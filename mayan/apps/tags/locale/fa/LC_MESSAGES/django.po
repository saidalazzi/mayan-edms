# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Translators:
# Roberto Rosario, 2023
# Mehdi Amani <MehdiAmani@toorintan.com>, 2023
# 
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-06-03 05:20+0000\n"
"PO-Revision-Date: 2023-01-05 02:55+0000\n"
"Last-Translator: Mehdi Amani <MehdiAmani@toorintan.com>, 2023\n"
"Language-Team: Persian (https://app.transifex.com/rosarior/teams/13584/fa/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fa\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: apps.py:46 apps.py:133 apps.py:137 apps.py:141 apps.py:146 apps.py:150
#: events.py:6 forms.py:27 links.py:28 menus.py:14 models.py:35
#: permissions.py:6 views.py:180 workflow_actions.py:18 workflow_actions.py:67
msgid "Tags"
msgstr "برچسب ها"

#: apps.py:171 models.py:29
msgid "Documents"
msgstr "اسناد"

#: events.py:10
msgid "Tag attached to document"
msgstr "برچسب متصل به سند"

#: events.py:13
msgid "Tag created"
msgstr ""

#: events.py:16
msgid "Tag edited"
msgstr ""

#: events.py:19
msgid "Tag removed from document"
msgstr "برچسب از سند حذف شد"

#: links.py:19 links.py:38
msgid "Attach tags"
msgstr "پیوست برچسب ها"

#: links.py:23 workflow_actions.py:74
msgid "Remove tag"
msgstr "برچسب را حذف کنید"

#: links.py:33
msgid "Remove tags"
msgstr "برچسب ها را حذف کنید"

#: links.py:44
msgid "Create new tag"
msgstr "ایجاد تگ جدید"

#: links.py:49 links.py:52
msgid "Delete"
msgstr "حذف"

#: links.py:57
msgid "Edit"
msgstr "ویرایش"

#: links.py:65
msgid "All"
msgstr "همه"

#: methods.py:19
msgid "Return the tags attached to the document."
msgstr ""

#: methods.py:21
msgid "get_tags()"
msgstr ""

#: model_mixins.py:63
msgid "Preview"
msgstr "پیش نمایش"

#: models.py:21
msgid "A short text used as the tag name."
msgstr ""

#: models.py:22
msgid "Label"
msgstr "برچسب"

#: models.py:25
msgid "The RGB color values for the tag."
msgstr ""

#: models.py:26
msgid "Color"
msgstr "رنگ"

#: models.py:34
msgid "Tag"
msgstr "برچسب"

#: models.py:63
msgid "Document tag"
msgstr "تگ سند"

#: models.py:64 search.py:24 search.py:30 search.py:36 search.py:42
msgid "Document tags"
msgstr "تگ های سند"

#: permissions.py:10
msgid "Create new tags"
msgstr "ایجاد برچسب های جدید"

#: permissions.py:13
msgid "Delete tags"
msgstr "برچسب ها را حذف کنید"

#: permissions.py:16
msgid "View tags"
msgstr "برچسب ها را مشاهده کنید"

#: permissions.py:19
msgid "Edit tags"
msgstr "ویرایش تگها"

#: permissions.py:22
msgid "Attach tags to documents"
msgstr "برچسب ها را به اسناد اضافه کنید"

#: permissions.py:25
msgid "Remove tags from documents"
msgstr "برچسب ها را از اسناد حذف کنید"

#: search.py:15
msgid "Tag label"
msgstr ""

#: search.py:18
msgid "Tag color"
msgstr ""

#: search.py:56
msgid "Document type"
msgstr "نوع سند"

#: search.py:59
msgid "Document label"
msgstr ""

#: search.py:62
msgid "Document description"
msgstr ""

#: search.py:65
msgid "Document UUID"
msgstr ""

#: search.py:69
msgid "Document file checksum"
msgstr ""

#: search.py:72
msgid "Document file MIME type"
msgstr ""

#: serializers.py:12
msgid "Documents URL"
msgstr ""

#: serializers.py:19
msgid "URL"
msgstr "نشانی اینترنتی"

#: serializers.py:33
msgid "Primary key of the tag to add to the document."
msgstr ""

#: serializers.py:34 serializers.py:43
msgid "Tag ID"
msgstr ""

#: serializers.py:42
msgid "Primary key of the tag to remove from the document."
msgstr ""

#: views.py:43
#, python-format
msgid "Tags attached to %(count)d documents successfully."
msgstr ""

#: views.py:46
#, python-format
msgid "Tags attached to document \"%(object)s\" successfully."
msgstr ""

#: views.py:49
#, python-format
msgid "Tags attached to %(count)d document successfully."
msgstr ""

#: views.py:51
#, python-format
msgid "Attach tags to %(count)d documents."
msgstr ""

#: views.py:52
#, python-format
msgid "Attach tags to document: %(object)s"
msgstr ""

#: views.py:53
#, python-format
msgid "Attach tags to %(count)d document."
msgstr ""

#: views.py:70 wizard_steps.py:28
msgid "Tags to be attached."
msgstr "برچسب ها باید متصل شوند"

#: views.py:108
msgid "Create tag"
msgstr "ایجاد تگ"

#: views.py:121
#, python-format
msgid "Error deleting tag \"%(instance)s\"; %(exception)s"
msgstr ""

#: views.py:126
#, python-format
msgid "%(count)d tags deleted successfully."
msgstr ""

#: views.py:127
#, python-format
msgid "Tag \"%(object)s\" deleted successfully."
msgstr ""

#: views.py:128
#, python-format
msgid "%(count)d tag deleted successfully."
msgstr ""

#: views.py:129
#, python-format
msgid "Delete the %(count)d selected tags"
msgstr ""

#: views.py:130
#, python-format
msgid "Delete tag: %(object)s"
msgstr ""

#: views.py:131
#, python-format
msgid "Delete the %(count)d selected tag"
msgstr ""

#: views.py:137
msgid "Will be removed from all documents."
msgstr "از همه مدارک حذف خواهد شد."

#: views.py:153
#, python-format
msgid "Edit tag: %s"
msgstr "ویرایش تگ: %s"

#: views.py:176
msgid ""
"Tags are color coded properties that can be attached or removed from "
"documents."
msgstr ""

#: views.py:179
msgid "No tags available"
msgstr ""

#: views.py:210
#, python-format
msgid "Documents with the tag: %s"
msgstr "اسناد با برچسب: %s"

#: views.py:237
msgid "Document has no tags attached"
msgstr ""

#: views.py:240
#, python-format
msgid "Tags for document: %s"
msgstr "برچسب ها برای سند: %s"

#: views.py:258
#, python-format
msgid "Tags removed from %(count)d documents successfully."
msgstr ""

#: views.py:261
#, python-format
msgid "Tags removed from document \"%(object)s\" successfully."
msgstr ""

#: views.py:264
#, python-format
msgid "Tags removed from %(count)d document successfully."
msgstr ""

#: views.py:266
#, python-format
msgid "Remove tags from %(count)d documents."
msgstr ""

#: views.py:267
#, python-format
msgid "Remove tags from document: %(object)s"
msgstr ""

#: views.py:268
#, python-format
msgid "Remove tags from %(count)d document."
msgstr ""

#: views.py:285
msgid "Tags to be removed."
msgstr "برچسب ها حذف می شوند"

#: wizard_steps.py:15
msgid "Select tags"
msgstr ""

#: workflow_actions.py:20
msgid "Tags to attach to the document"
msgstr "برچسب ها برای پیوستن به سند"

#: workflow_actions.py:25
msgid "Attach tag"
msgstr "ضمیمه برچسب"

#: workflow_actions.py:69
msgid "Tags to remove from the document."
msgstr ""
