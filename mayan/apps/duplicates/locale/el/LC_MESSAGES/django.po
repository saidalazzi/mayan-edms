# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Translators:
# pyrotech <anastasios@tutamail.com>, 2023
# Hmayag Antonian <hmayag@freemail.gr>, 2023
# 
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-06-03 05:20+0000\n"
"PO-Revision-Date: 2023-01-05 02:52+0000\n"
"Last-Translator: Hmayag Antonian <hmayag@freemail.gr>, 2023\n"
"Language-Team: Greek (https://app.transifex.com/rosarior/teams/13584/el/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: el\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: apps.py:28 apps.py:54 links.py:14 queues.py:9
msgid "Duplicates"
msgstr "Διπλότυπα"

#: apps.py:60
msgid "Duplicate backend"
msgstr ""

#: classes.py:90
msgid "Null backend"
msgstr "Υποκείμενο σύστημα κενής τιμής"

#: duplicate_backends.py:9
msgid "Exact document file checksum"
msgstr ""

#: duplicate_backends.py:32
msgid "Exact document label"
msgstr ""

#: links.py:18 models.py:37 views.py:79
msgid "Duplicated documents"
msgstr "Διπλοκαταχωρημένα έγγραφα"

#: links.py:24 queues.py:23
msgid "Duplicated document scan"
msgstr "Ανίχνευση για διπλές καταχωρήσεις εγγράφων"

#: models.py:19 models.py:29
msgid "Stored duplicate backend"
msgstr ""

#: models.py:20
msgid "Stored duplicate backends"
msgstr ""

#: models.py:33
msgid "Document"
msgstr "Έγγραφο"

#: models.py:45
msgid "Duplicated backend entry"
msgstr ""

#: models.py:46
msgid "Duplicated backend entries"
msgstr ""

#: queues.py:14
msgid "Clean empty duplicate lists"
msgstr "Εκκαθάριση κενής λίστας διπλότυπων"

#: queues.py:18
msgid "Scan document duplicates"
msgstr "Σάρωση διπλοκαταχωρημένων εγγράφων"

#: views.py:36
msgid "Only exact copies of this document will be shown in the this list."
msgstr ""
"Μόνο ακριβή αντίγραφα αυτού του εγγράφου θα εμφανίζονται σε αυτήν τη λίστα."

#: views.py:40
msgid "There are no duplicates for this document"
msgstr "Δεν υπάρχουν διπλότυπα για αυτό το έγγραφο"

#: views.py:44
#, python-format
msgid "Duplicates for document: %s"
msgstr "Διπλότυπα του εγγράφου: %s"

#: views.py:70
msgid ""
"Duplicates are documents that are composed of the exact same file, down to "
"the last byte. Files that have the same text or OCR but are not identical or"
" were saved using a different file format will not appear as duplicates."
msgstr ""
"Τα διπλότυπα είναι έγγραφα που αποτελούνται από το ίδιο ακριβώς αρχείο, "
"μέχρι το τελευταίο byte. Τα αρχεία που έχουν το ίδιο κείμενο ή OCR αλλά δεν "
"είναι πανομοιότυπα ή έχουν αποθηκευτεί χρησιμοποιώντας διαφορετική μορφή "
"αρχείου δεν θα εμφανίζονται ως διπλότυπα."

#: views.py:77
msgid "There are no duplicated documents"
msgstr "Δεν υπάρχουν διπλότυπα έγγραφα"

#: views.py:87
msgid "Scan for duplicated documents?"
msgstr "Αναζήτηση για διπλότυπα έγγραφα;"

#: views.py:95
msgid "Duplicated document scan queued successfully."
msgstr ""
"Αίτημα αναζήτησης για διπλότυπα έγγραφα καταχωρήθηκε στην λίστα με επιτυχία."
